#include "MagshDocument.h"
#include <fstream>

const std::string MagshDocument::FILE_PATH = "shared_doc.txt";


MagshDocument::MagshDocument()
{

}

MagshDocument::~MagshDocument()
{
}

// the funcion reads content of shared file
// if the file does not exist return empty string
// the function is thread-safe: use mutex to lock the file
std::string MagshDocument::read()
{
	const int SIZE = 100;

	std::ifstream doc;
	std::string res;


	doc.open(FILE_PATH.c_str());
		

	if (!doc.is_open())
	{
		return "";
	}

	std::string line;
	while (!doc.eof())
	{
		std::getline(doc, line);
		//doc.getline(output, SIZE);
		res += line;
		if (!doc.eof())
		{
			res.append("\n\r");
		}
	}

	doc.close();

	return res;
}



// the funcion write content to shared file
// creates new file even if the file exists
// the function is thread-safe: use mutex to lock the file
void MagshDocument::write(const std::string& data)
{
	std::ofstream doc;

	doc.open(FILE_PATH.c_str(), ios::out);

	doc.write(data.c_str(), data.size());
	doc.flush();
	doc.close();
}
