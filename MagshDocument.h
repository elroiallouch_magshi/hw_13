#pragma once

#include "Helper.h"
#include <string>
#include <mutex>

// Q: why do we need this class ?
// A: a wrap to std::fstream for our need
class MagshDocument
{
public:
	static const std::string FILE_PATH;

	MagshDocument();
	~MagshDocument();
	std::string read();
	void write(const std::string& data);

private:
};

